﻿# is this necessary?

no

# so why does it exists?

because I want to

# oh okay fuck you too

thanks

# core game loop
## musings

I'm not sure yet of how does the game end. Is the player able to actually loose? does it end when that happens or it does go through some minigames and then it ends by itself?

I don't think the game is actually gonna have any real objective. I want to add points and what not but it is mostly just to make it look like they matter but actually being completely meaningless. It would be fun to see people competing to get highscores at sex, I mean SEX

## pick you SEX duration
i guess I could make it so that you pick a SEX length and then the game goes on for that set amount of minigames and then it is over. I could then theoretically save a highscore and stuff.

I'm inclined to go with it because I could pick very silly names for fun for each length. like the shortest one being "McQueen mode" and so on.

then, the core loop would be somewhat like this:

- menu → pick → length → play minigames → highscore gallery (by length?) → credits I think → menu

fairly simple and straight forward.

# graphics
really simple gba-like sprites using my signature colors. it is pretty accessible, in the way of it being accessible for me to produce it, for me to convey stuff and for people that are somewhat familiar with games to understand. not that I can do any other style of art properly but it doesn't really matter.

also to keep the spirit of the game of being purposefully weird the art will be somewhat unclear. sure, that does look like a penis but it sill sometimes there will be a lot of them crossing the screen and you gotta get to the other side and what the fuck is going on what even is anything.

and stuff like that

# sound
I'll do all the sounds using my own mouth. I'll also try and write a song called "the SEX song" which will be the background song but if I'm able to I'll play it with lyrics over the credits and it will be really fucking bad and glorious.

the point is for it to be bad and thus funny (in my opinion)

also the sounds are gonna be randomized just to fuck everything up. I can't wait for a run where everything makes fart noises I'll probably win an award.

# the minigames
the minigames are gonna be greatly varied. I want to add heavy randomization in the objects involved and the sounds they make so they look different each time and, I'm hoping, off and hilarious

## ruba a rub dub dub
rub one thing into another until SEX is achieved.

## hard rub a rub dub dub
the same as above but there are some obstacles in the way. avoid then or don't I'm not your mom

## fire in the hole
shove something into a suspicious looking hole... or object.

## fires in the hole
shove it many times for some extra SEX

## maze
guide the thing to the other thing

## it is raining stuff
get the raining stuff

## it is raining stuff but some of it is bad
get the raining stuff but avoid the other

## it is raining lots of stuff and all of it is good I just wanna fuck with you
yeah

## bust a nut
just blast something with a bat

## bust a move
just move around touching everything you can

## dark
the screen is dark and nothing happens

## dark but its a dance party
the screen is dark but pressing keys gets you more SEX

## jerk it
just jerk it but it is a random thing

## circles
just circle it but it is a random thing

## talk
there is a text box type something in it doesn't matter what tho (or it does as a easter egg). each typed character plays a random sound

## balloon
inflate something

## extreme balloon
inflate something till it goes pop

# modifiers

I've come to conclusion that while I do have many bad ideas for minigames, some are just modifiers and should be applied randomly on stuff

## tiny
stuff is small

## giga
stuff is big

## furry
all sounds are animal sounds

## watch
something is happening but you have no control over it

## watch and jerk it
something is happening and it will keep happening but you can jerk some random thing at least

## blindfold
everything is invisible, good luck

## brat
do the opposite of what you're supposed to do to get SEX

## eyes
nothing they're just watching

## rubber
everything makes squeaky sounds
